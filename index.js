let http = require("http");
let port = 4000;
let server = http.createServer((req, res) => {
	// 1. GET
	if(req.url == "/items" && req.method == "GET"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Data retrieved from the database.");
	}

	// 2. POST 
	if(req.url == "/items" && req.method == "POST"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Data to be sent to the database");
	}
});

server.listen(port);
console.log(`Server is running at localhost: ${port}.`);


/*
HTTP METHODS 
1. GET = retrieve/read resources/information
2. POST = sends/insert data for creating a /in the server or DB
3. PUT = senda data for updating a resource
4. DELETE = deletes a specified resource
*/